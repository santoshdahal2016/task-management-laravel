<?php

namespace App\Http\Controllers\Auth;

use App\Modules\User\Models\Role;
use App\Modules\User\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\SignupActivate;
use App\Modules\User\Resource\User as UserResource ;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $data
     * @return UserResource
     */
    protected function create(Request $data)
    {

        $this->validate($data, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        $user = new User([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'activation_token' => str_random(5),
            'active' => "inactive"
        ]);

        $user->save();
        $role = Role::where('name','customer')->first();
        $user->roles()->attach($role->id);

       if(Config::get('auth.authentication') == "loose"){
           $user->active = "active";
           $user->activation_token = "";
           $user->save();
       }elseif (Config::get('auth.authentication') == "code"){
           $user->active = "active";
           $user->save();
           $user->notify(new SignupActivate($user));

       }elseif (Config::get('auth.authentication') == "admin"){
           $user->activation_token = "";
           $user->save();
       }elseif (Config::get('auth.authentication') == "hybrid")
       {
           $user->notify(new SignupActivate($user));
       }


        return new  UserResource($user);

    }

    public function verify(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users',
            'code' => 'required|min:5',
        ]);
        $user = User::where('activation_token', $request->code)->where('email',$request->email)->first();

        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->activation_token = '';
        $user->save();

        return new UserResource($user);
    }


}
