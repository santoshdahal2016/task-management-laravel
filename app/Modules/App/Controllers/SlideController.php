<?php

namespace App\Modules\App\Controllers;

use App\Modules\App\Models\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\App\Resource\Slide as SlideResource;
use Illuminate\Support\Facades\Auth;

class SlideController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {

        if ($request->has('title') && $request->has('limit') && $request->has('sort')) {
            $slide = Slide::where('title', 'like', '%' . $request->title . '%')->orderBy('id', $request->sort)->paginate($request->input('limit'));
            return SlideResource::collection($slide);
        } elseif ($request->has('limit') && $request->has('sort')) {
            $slide = Slide::orderBy('id', $request->sort)->paginate($request->input('limit'));
            return SlideResource::collection($slide);
        }
        return SlideResource::collection(Slide::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return SlideResource
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $this->validate($request, [
                'title' => 'required',
                'link' => 'required',
            ]);
            $slide = Slide::create($request->all());
            if ($request['image'] != null) {

                $slide->clearMediaCollection('image');
                $slide->addMediaFromRequest('image')->toMediaCollection('image');

            }
            return new  SlideResource($slide);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return SlideResource
     */
    public function show($id)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            return new SlideResource(Slide::find($id));
        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return SlideResource
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $slide = Slide::findOrFail($id);


            $slide->fill($request->all())->save();
            if ($request['image'] != null) {

                $slide->clearMediaCollection('image');
                $slide->addMediaFromRequest('image')->toMediaCollection('image');

            }
            return new  SlideResource($slide);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            Slide::whereId($id)->delete();
            $return = ["status" => "Success",
                "error" => [
                    "code" => 200,
                    "errors" => 'Deleted'
                ]];
            return response()->json($return, 200);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }
}
