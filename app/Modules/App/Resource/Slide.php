<?php

namespace App\Modules\App\Resource;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Product\Resource\Product as ProductResource ;

class Slide extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'link' => $this->link,
            'image' => $this->when(1, function () {
                if (count($this->getMedia('image')) > 0) {
                    return $this->getMedia('image')[0]->getFullUrl();

                } else {
                    return "https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png";
                }
            }),
        ];
    }
}
