<?php

namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Slide extends Model implements HasMedia {

    use  HasMediaTrait ;

    protected $fillable = [
        'title', 'link'
    ];
}
