<?php

Route::group(['module' => 'App', 'prefix'=>'api','middleware' => ['cors'],'namespace' => 'App\Modules\App\Controllers'], function() {

    Route::get('/slide', 'SlideController@index');

});

Route::group(['module' => 'App', 'prefix'=>'api','middleware' => ['cors','api','auth:api'],'namespace' => 'App\Modules\App\Controllers'], function() {

    Route::resource('slide', 'SlideController', array('except' => array('index')));

});
