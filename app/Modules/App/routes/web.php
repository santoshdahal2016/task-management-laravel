<?php

Route::group(['module' => 'App', 'middleware' => ['web'], 'namespace' => 'App\Modules\App\Controllers'], function() {

    Route::resource('app', 'AppController');

});
