<?php

Route::group(['module' => 'Category', 'prefix'=>'api','middleware' => ['cors','api','auth:api'], 'namespace' => 'App\Modules\Category\Controllers'], function() {

    Route::resource('category', 'CategoryController');
    Route::post('menu/edit',  'CategoryController@menu_edit');


});

Route::group(['module' => 'Category', 'prefix'=>'api','middleware' => ['cors'], 'namespace' => 'App\Modules\Category\Controllers'], function() {

    Route::get('menu',  'CategoryController@menu');
    Route::get('/category/product/{name}',  'CategoryController@product');


});