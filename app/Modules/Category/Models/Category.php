<?php

namespace App\Modules\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia {

    use HasMediaTrait;
    protected $fillable = [
        'title', 'parent_id'
    ];


    public function children() {
        return $this->hasMany('App\Modules\Category\Models\Category','parent_id');
    }
    public function parent() {
        return $this->belongsTo('App\Modules\Category\Models\Category','parent_id');
    }

    public function products() {
        return $this->belongsToMany('App\Modules\Product\Models\Product','products_categories','category_id','product_id');
    }

}
