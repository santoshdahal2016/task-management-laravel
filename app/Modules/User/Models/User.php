<?php

namespace App\Modules\User\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class User extends Authenticatable implements HasMedia
{

    use EntrustUserTrait , HasApiTokens, Notifiable , HasMediaTrait ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'password','active', 'activation_token','provider_name', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Modules\User\Models\Role');

    }

    public function report() {
        return $this->hasMany('App\Modules\Report\Models\Report', 'user_id');
    }

    public function reportByDate($date) {
        return $this->report()->whereDate('date', '=', $date)->with('task');
    }



}

