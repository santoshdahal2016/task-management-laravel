<?php

Route::group(['module' => 'Report', 'prefix'=>'api','middleware' => ['cors','api','auth:api'], 'namespace' => 'App\Modules\Report\Controllers'], function() {
    Route::post('/reportuser/{id}', 'ReportController@user');

    Route::resource('report', 'ReportController');
    Route::get('myreport', 'ReportController@myReport');

    Route::get('/{filter}/report/time', 'ReportController@time');
    Route::get('/totalbusiness', 'ReportController@totalBusiness');

});
