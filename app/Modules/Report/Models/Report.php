<?php

namespace App\Modules\Report\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

    protected $fillable = [
        'user_id', 'task_id','count','amount','date'
    ];

    public function user() {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function task() {
        return $this->belongsTo('App\Modules\Task\Models\Task');
    }
}
