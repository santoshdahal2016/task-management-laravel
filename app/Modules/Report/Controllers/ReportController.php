<?php

namespace App\Modules\Report\Controllers;

use App\Modules\Report\Models\Report;
use App\Modules\Task\Models\Task;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Report::index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        foreach ($request->report as $item) {
            $task = Task::findOrFail($item['task_id']);
            $item['amount'] = $item['count'] * $task->price;
            $item['user_id'] = $user->id;
            $item['date'] = date('Y-m-d');
            $report = Report::where('user_id', $item['user_id'])->where('task_id', $item['task_id'])->where('date', $item['date'])->first();
            if ($report) {
                $report->fill($item)->save();
            } else {
                Report::create($item);
            }

        }
        return $user->reportByDate($item['date'])->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function time($filter)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {

            if ($filter === "yearly") {
                $data1['categories'] = Report::select(
                    DB::raw('sum(amount) as total'),
                    DB::raw("DATE_FORMAT(date,'%Y') as years")
                )
                    ->groupBy('years')
                    ->pluck('years');

                $data2['xaxis'] = $data1;

                $data3['id'] = "vuechart";
                $data2['chart'] = $data3;

                $data['name'] = "Yearly Income";
                $data['data'] = Report::select(
                    DB::raw('sum(amount) as total'),
                    DB::raw("DATE_FORMAT(date,'%Y') as years")
                )
                    ->groupBy('years')
                    ->pluck('total');
                $report['series'][] = $data;
                $report['bardata'] = $data2;

                return response()->json($report);
            } elseif ($filter === "monthly") {
                $data1['categories'] = Report::select(
                    DB::raw('sum(amount) as total'),
                    DB::raw("DATE_FORMAT(date,'%M %Y') as months")
                )
                    ->groupBy('months')
                    ->pluck('months');

                $data2['xaxis'] = $data1;

                $data3['id'] = "vuechart";
                $data2['chart'] = $data3;

                $data['name'] = "Monthly Income";
                $data['data'] = Report::select(
                    DB::raw('sum(amount) as total'),
                    DB::raw("DATE_FORMAT(date,'%M %Y') as months")
                )
                    ->groupBy('months')
                    ->pluck('total');
                $report['series'][] = $data;
                $report['bardata'] = $data2;

                return response()->json($report);
            }
        }
    }

    public function user(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('leader')) {
            $user = User::where('id', $id)->first();
            return $user->reportByDate($request->date)->get();
        }
    }

    public function myReport()
    {
        $user = Auth::user();
        return $user->reportByDate(date('Y-m-d'))->get();
    }

    public function totalBusiness()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {

            return Report::sum('amount');
        }
    }
}
