<?php

namespace App\Modules\Task\Resource;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Category\Resource\TempCategory as TempCategoryResource;
class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,

        ];
    }
}
