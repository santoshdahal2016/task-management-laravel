<?php

namespace App\Modules\Task\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $fillable = [
        'title', 'price'
    ];

    public function report() {
        return $this->hasMany('App\Modules\Report\Models\Report', 'task_id');
    }

}
