<?php

Route::group(['module' => 'Task', 'prefix'=>'api','middleware' => ['cors','api','auth:api'], 'namespace' => 'App\Modules\Task\Controllers'], function() {

    Route::resource('task', 'TaskController');

});
