<?php

namespace App\Modules\Task\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Task\Models\Task as Task;
use App\Modules\Task\Resource\Task as TaskResource;

class TaskController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($request->has('title') && $request->has('limit') && $request->has('sort')) {
            $task = Task::where('title', 'like', '%' . $request->title . '%')->orderBy('id', $request->sort)->paginate($request->input('limit'));
            return TaskResource::collection($task);
        } elseif ($request->has('limit') && $request->has('sort')) {
            $task = Task::orderBy('id', $request->sort)->paginate($request->input('limit'));
            return TaskResource::collection($task);
        }
        return TaskResource::collection(Task::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return TaskResource
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('admin') || $user->hasRole('leader')) {
            $this->validate($request, [
                'title' => 'required',
                'price' => 'required|numeric'

            ]);

            $task = Task::create($request->all());

            return new  TaskResource($task);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return TaskResource
     */
    public function show($id)
    {
        $user = Auth::user();

        if ($user->hasRole('admin') || $user->hasRole('leader')) {
            return new TaskResource(Task::find($id));
        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return TaskResource
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('leader')) {
            $task = Task::findOrFail($id);
            $task->fill($request->all())->save();
            $task = Task::findOrFail($id);

            return new  TaskResource($task);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            Task::whereId($id)->delete();
            $return = ["status" => "Success",
                "error" => [
                    "code" => 200,
                    "errors" => 'Deleted'
                ]];
            return response()->json($return, 200);

        } else {
            $return = ["status" => "error",
                "error" => [
                    "code" => 403,
                    "errors" => 'Forbidden'
                ]];
            return response()->json($return, 403);
        }
    }
}
