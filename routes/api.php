<?php


use Illuminate\Support\Facades\Config;

Route::middleware('auth:api')->get('/logout', 'Api\LoginController@logout');
Route::middleware('auth:api')->get('/auth/user', 'Api\LoginController@user');

Route::post('/login', 'Api\LoginController@login');
Route::post('/signup', 'Auth\RegisterController@create');
Route::post('/verify', 'Auth\RegisterController@verify');
Route::post('/social', 'Api\LoginController@social');

Route::get('/', function () {
    $return = [
        "title" => "TASK MANAGEMENT",
        "description" => "This Manage task",
        "authentication" => Config::get('auth.authentication'),
        "socialLogin" => False

    ];
    return response()->json($return, 200);
});