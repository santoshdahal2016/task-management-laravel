<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => 'This account is for Admin',
            ],
            [
                'name' => 'user',
                'display_name' => 'User',
                'description' => 'This account is for User',
            ]
            ];

        DB::table('roles')->insert($roles);


        $role_user = [
            [
                'user_id' => '1',
                'role_id' => '1',
            ],
            [
                'user_id' => '2',
                'role_id' => '2',
            ]


        ];

        //insert base data in role_user table

        DB::table('role_user')->insert($role_user);


        for ($i = 3; $i < 10; $i++) {
            DB::table('role_user')->insert([
                'user_id' => $i,
                'role_id' => '2'
            ]);

        }
    }
}
